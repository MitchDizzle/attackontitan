#include <sdkhooks>
#include <sdktools>

//#include <AttackOnTitan>

#define MAXGAS 16000


new GetVelocityOffset_x;
new GetVelocityOffset_y;
new GetVelocityOffset_z;

new precache_laser;

//models\weapons\c_models\c_bet_rocketlauncher\c_bet_rocketlauncher.mdl

new eRope[MAXPLAYERS+1][4];

new iGas[MAXPLAYERS+1] = {MAXGAS,...};

public OnPluginStart()
{
	GetVelocityOffset_x = FindSendPropOffs("CTFPlayer", "m_vecVelocity[0]");
	if(GetVelocityOffset_x == -1)
		SetFailState("[MG] Error: Failed to find the velocity_x offset, aborting");

	GetVelocityOffset_y = FindSendPropOffs("CTFPlayer", "m_vecVelocity[1]");
	if(GetVelocityOffset_y == -1)
		SetFailState("[MG] Error: Failed to find the velocity_y offset, aborting");

	GetVelocityOffset_z = FindSendPropOffs("CTFPlayer", "m_vecVelocity[2]");
	if(GetVelocityOffset_z == -1)
		SetFailState("[MG] Error: Failed to find the velocity_z offset, aborting");
		
	HookEvent("player_spawn", Event_Spawn);
	HookEvent("player_death", Event_Death);
	for (new i = 1; i <= MaxClients; i++) 
	{
		if (IsClientInGame(i) && IsPlayerAlive(i))
		{
			CreateMGear(i);
		}
	}
}
//setpos 2180.264893 -9703.968750 516.031311;setang 65.941559 90.862434 0.000000
// [||||||||||]

public OnPluginEnd()
{
	for (new i = 1; i <= MaxClients; i++) 
	{
		if (IsClientInGame(i))
		{
			RemoveMGear(i);
		}
	}
}
public Action:Event_Spawn(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if(IsPlayerAlive(client))
		CreateMGear(client);
}
public Action:Event_Death(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	RemoveMGear(client);
}
CreateMGear(client)
{
	RemoveMGear(client);
	eRope[client][0] = CreateEntityByName("env_beam");
	if (eRope[client][0] != -1)
	{
		SetEntityModel(eRope[client][0], "materials/sprites/laserbeam.vmt");
		DispatchKeyValue(eRope[client][0], "targetname", "rope0");
		DispatchKeyValue(eRope[client][0], "rendercolor", "0 0 0");
		DispatchKeyValue(eRope[client][0], "renderamt", "0");
		DispatchSpawn(eRope[client][0]);
		
		SetEntPropFloat(eRope[client][0], Prop_Send, "m_fWidth", 0.0);
		SetEntPropFloat(eRope[client][0], Prop_Send, "m_fEndWidth", 0.0);
		ActivateEntity(eRope[client][0]);
	}
	eRope[client][1] = CreateEntityByName("info_particle_system");
	if (eRope[client][1] != -1)
	{
		DispatchKeyValue(eRope[client][1], "targetname", "target1");
		DispatchSpawn(eRope[client][1]);
	}
	eRope[client][2] = CreateEntityByName("env_beam");
	if (eRope[client][2] != -1)
	{
		SetEntityModel(eRope[client][2], "materials/sprites/laserbeam.vmt");
		DispatchKeyValue(eRope[client][2], "rendercolor", "0 0 0");
		DispatchKeyValue(eRope[client][2], "renderamt", "0");
		DispatchSpawn(eRope[client][2]);
	
		ActivateEntity(eRope[client][2]);
		AcceptEntityInput(eRope[client][2], "TurnOn");
	}
	
	SetVariantString("!activator");
	AcceptEntityInput(eRope[client][2], "SetParent", eRope[client][1], eRope[client][2], 0);
	
	SetVariantString("!activator");
	AcceptEntityInput(eRope[client][1], "SetParent", client, eRope[client][1], 0);
	SetVariantString("flag");
	AcceptEntityInput(eRope[client][1], "SetParentAttachment", eRope[client][1], eRope[client][1], 0);
}
RemoveMGear(client)
{
	if(IsRope(eRope[client][0]))
	{
		AcceptEntityInput(eRope[client][0],"Kill");
		eRope[client][0] = -1;
	}
	if(IsTarget(eRope[client][1]))
	{
		AcceptEntityInput(eRope[client][1],"Kill");
		eRope[client][1] = -1;
	}
	if(IsTarget2(eRope[client][2]))
	{
		AcceptEntityInput(eRope[client][2],"Kill");
		eRope[client][2] = -1;
	}
}
public OnMapStart()
{
	PrecacheModel("materials/sprites/laserbeam.vmt");
	precache_laser = PrecacheModel("materials/cable/white.vmt");
	CreateTimer(1.0, Timer_UpdateInfo, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
}
public Action:Timer_UpdateInfo(Handle:timer)
{
	new String:sGas[24];
	for(new i = 1; i <= MaxClients; i++) {
		if(IsClientInGame(i))
		{
			SetHudTextParams(0.145, 1.0, 1.0, 255, 255, 255, 255, 0, 0.0, 0.0, 0.0);
			ShowHudText(i, 2, "[                  ]");
			GetPercentBar((float(iGas[i])*100.0)/float(MAXGAS), sGas);
			SetHudTextParamsEx(0.15, 1.0, 1.0, (iGas[i] <= (MAXGAS*25)/100) ? {255, 64, 64, 255} : (iGas[i] <= (MAXGAS*50)/100) ? {255, 192, 64, 255} : (iGas[i] <= (MAXGAS*75)/100) ? {255, 255, 64, 255} : {64, 255, 64, 255}, (iGas[i] <= (MAXGAS*25)/100) ? {255, 0, 0, 255} : (iGas[i] <= (MAXGAS*50)/100) ? {255, 128, 0, 255} : (iGas[i] <= (MAXGAS*75)/100) ? {255, 255, 0, 255} : {0, 255, 0, 255}, 2, 0.5, 0.01, 0.01);
			ShowHudText(i, 1, sGas);
		}
	}
}
stock GetPercentBar(Float:iPercent=100.0, String:sReturn[24])
{
	new iNum = RoundToNearest(iPercent)
	if(iNum > 95)		Format(sReturn, sizeof(sReturn), "||||||||||");
	else if(iNum > 90)  Format(sReturn, sizeof(sReturn), "|||||||||:");
	else if(iNum > 85)  Format(sReturn, sizeof(sReturn), "|||||||||");
	else if(iNum > 80)  Format(sReturn, sizeof(sReturn), "||||||||:");
	else if(iNum > 75)  Format(sReturn, sizeof(sReturn), "||||||||");
	else if(iNum > 70)  Format(sReturn, sizeof(sReturn), "|||||||:");
	else if(iNum > 65)  Format(sReturn, sizeof(sReturn), "|||||||");
	else if(iNum > 60)  Format(sReturn, sizeof(sReturn), "||||||:");
	else if(iNum > 55)  Format(sReturn, sizeof(sReturn), "||||||");
	else if(iNum > 50)  Format(sReturn, sizeof(sReturn), "|||||:");
	else if(iNum > 45)  Format(sReturn, sizeof(sReturn), "|||||");
	else if(iNum > 40)  Format(sReturn, sizeof(sReturn), "||||:");
	else if(iNum > 35)  Format(sReturn, sizeof(sReturn), "||||");
	else if(iNum > 30)  Format(sReturn, sizeof(sReturn), "|||:");
	else if(iNum > 25)  Format(sReturn, sizeof(sReturn), "|||");
	else if(iNum > 20)  Format(sReturn, sizeof(sReturn), "||:");
	else if(iNum > 15)  Format(sReturn, sizeof(sReturn), "||");
	else if(iNum > 10)  Format(sReturn, sizeof(sReturn), "|:");
	else if(iNum > 5)   Format(sReturn, sizeof(sReturn), "|");
	else if(iNum > 0)   Format(sReturn, sizeof(sReturn), ":");
	else 				Format(sReturn, sizeof(sReturn), "  ! REFILL ! ");
}
public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	static bool:PressingHook[MAXPLAYERS + 1] = {false,...};
	static Float:HookPos[MAXPLAYERS + 1][3];
	static Float:HookDist[MAXPLAYERS + 1];

	if (buttons & IN_ATTACK2)
	{
		if(IsPlayerAlive(client) && iGas[client] > 0)
		{
			new Float:velocity[3];
			decl Float:CPos[3], Float:dir[3];
			GetEntPropVector(client, Prop_Send, "m_vecOrigin", CPos);
			CPos[2] += 20.0;
			GetVelocity(client, velocity);
			
			if(!PressingHook[client])
			{
				TraceEye(client, HookPos[client]);
				ScaleVector(velocity, 0.5);
				TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, velocity);
				//TE_SetupMetalSparks(CPos, CPos);
				//TE_SendToAll();
				TeleportEntity(eRope[client][0], HookPos[client], NULL_VECTOR, NULL_VECTOR);
			}
			TE_SetupBeamLaser(eRope[client][0], eRope[client][2], precache_laser, 0, 0, 0, 0.1, 0.1, 0.1, 0, 0.0, {0, 0, 0, 255}, 1);
			TE_SendToAll();
			HookDist[client] = GetVectorDistance(CPos, HookPos[client]);
			if(HookDist[client] <=100.0)
				ScaleVector(velocity, 0.6);
			else
				iGas[client]--;
			
			SubtractVectors(HookPos[client], CPos, dir);
			NormalizeVector(dir, dir);
			ScaleVector(dir, 25.0);
			dir[2] += 6.0*1.5;
			if(dir[2] < 0)
				dir[2] += dir[2]/100;
			velocity[0] += dir[0];
			velocity[1] += dir[1];
			velocity[2] += dir[2];

			if(HookDist[client] <=25.0)
				ScaleVector(velocity, 0.5);

			TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, velocity);
			
			//BeamEffect(CPos, HookPos[client], 0.1, 1.0, 1.0, {255,255,255,255}, 0.0, 1);
			PressingHook[client] = true;
		}
	}
	else 
	{
		PressingHook[client] = false;
		if(iGas[client] < MAXGAS)
			iGas[client]++;
	}
}
stock TraceEye(client, Float:pos[3])
{
	decl Float:vAngles[3], Float:vOrigin[3];
	GetClientEyePosition(client, vOrigin);
	GetClientEyeAngles(client, vAngles);
	TR_TraceRayFilter(vOrigin, vAngles, MASK_PLAYERSOLID, RayType_Infinite, TraceEntityFilterPlayer);
	if(TR_DidHit(INVALID_HANDLE))
	{
		TR_GetEndPosition(pos, INVALID_HANDLE);
		return true;
	}
	return false;
}

public bool:TraceEntityFilterPlayer(entity, contentsMask)
{
	return (entity > MaxClients || !entity);
}
public GetVelocity(client, Float:output[3])
{
	output[0] = GetEntDataFloat(client, GetVelocityOffset_x);
	output[1] = GetEntDataFloat(client, GetVelocityOffset_y);
	output[2] = GetEntDataFloat(client, GetVelocityOffset_z);
}

stock bool:IsRope(Ent)
{
	if(Ent != -1)
	{
		if(IsValidEdict(Ent) && IsValidEntity(Ent) && IsEntNetworkable(Ent))
		{
			decl String:ClassName[255];
			GetEdictClassname(Ent, ClassName, 255);
			if(StrEqual(ClassName, "env_beam"))
			{
				return (true);
			}
		}
	}
	return (false);
}
stock bool:IsTarget(Ent)
{
	if(Ent != -1)
	{
		if(IsValidEdict(Ent) && IsValidEntity(Ent) && IsEntNetworkable(Ent))
		{
			decl String:ClassName[255];
			GetEdictClassname(Ent, ClassName, 255);
			if(StrEqual(ClassName, "info_particle_system"))
			{
				return (true);
			}
		}
	}
	return (false);
}
stock bool:IsTarget2(Ent)
{
	if(Ent != -1)
	{
		if(IsValidEdict(Ent) && IsValidEntity(Ent) && IsEntNetworkable(Ent))
		{
			decl String:ClassName[255];
			GetEdictClassname(Ent, ClassName, 255);
			if(StrEqual(ClassName, "env_laser"))
			{
				return (true);
			}
		}
	}
	return (false);
}