/**
 * Attack On Titan RPG
 * Version 1.0.0
 *
 *
 *
 *
**/
#include <sdkhooks>
#include <sdktools>

#include <AttackOnTitan>

#define VERSION "1.0.0"

new iCurrency[MAXPLAYERS+1] = {0,...};

public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
	CreateNative("AOT_GetCash", Native_GetCash);
	CreateNative("AOT_SetCash", Native_SetCash);
	
	return APLRes_Success;
}

public OnPluginStart()
{
	//Events
	HookEvent("player_spawn", Event_Spawn);
	HookEvent("player_death", Event_Death);
	
	//Commands
	AddCommandListener(MedicHook, "voicemenu");
	
	//Lateloading.
	for (new i = 1; i <= MaxClients; i++) 
	{
		if (IsClientInGame(i) && IsPlayerAlive(i))
		{
			//CreateMGear(i);
			DefaultPlayer(i)
		}
	}
}

DefaultPlayer(client)
{
	iCurrency[client] = 0;
}

public Action:Event_Spawn(Handle:event, const String:name[], bool:dontBroadcast)
{
	/*new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if(IsPlayerAlive(client))
	{
		
	}*/
}
public Action:Event_Death(Handle:event, const String:name[], bool:dontBroadcast)
{
	//new client = GetClientOfUserId(GetEventInt(event, "userid"));
	//RemoveMGear(client);
}
public OnMapStart()
{
	CreateTimer(1.0, Timer_UpdateInfo, _, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
}


public Action:Timer_UpdateInfo(Handle:timer)
{
	for(new i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i))
		{
			SetHudTextParams(0.15, 0.92, 1.0, 0, 255, 0, 255, 0, 0.0, 0.0, 0.0);
			ShowHudText(i, 3, "$ %d", iCurrency[i]);
		}
	}
}


// Functions for players...
public Action:MedicHook(client, const String:cmd[], args)
{
	new String:arg1[2],
		String:arg2[2];

	GetCmdArg(1, arg1, sizeof(arg1));
	GetCmdArg(2, arg2, sizeof(arg2));


	if (StrEqual(arg1, "0") && StrEqual(arg2, "0"))
	{
		if (!IsFakeClient(client))
		{
			new Ent = TraceToEntity(client, 100.0);
			if(Ent!=-1 && IsValidEntity(Ent))
			{
				new String:Class[65];
				GetEdictClassname(Ent, Class, sizeof(Class));
				if(StrContains(Class,"dispenser") != -1)
				{
					//AcceptEntityInput(Ent, "Press", client);
					
					//SDKHooks_TakeDamage(Ent, client, client, 1.0);
				}
			}
		}
	}
	return Plugin_Continue;
}


//-------------------------------
// Natives!
//-------------------------------

public Native_GetCash(Handle:plugin, numParams)
{
	return iCurrency[GetNativeCell(1)];
}

public Native_SetCash(Handle:plugin, numParams)
{
	return iCurrency[GetNativeCell(1)] = GetNativeCell(2);
}