
#if defined _aot_included
 #endinput
#endif
#define _aot_included

enum AOT_Class
{
	AOT_None = 0,
	AOT_Soldier,
	AOT_Demo,
	AOT_Infantry,	//Sniper.
	AOT_ClassOne,
	AOT_ClassTwo,
	AOT_ClassThree,
	AOT_ClassFour,
	AOT_Shifter //Admins?
};

enum AOT_Team
{
	AOT_None = 0,
	AOT_Human,
	AOT_Titan
};



/**
 * Retrieves the class the player is.
 *
 * @param client		Client index.
 * @return				AoT class index
 */
native AOT_Class:AOT_GetClass(client);

/**
 * Retrieves the team the player is on.
 *
 * @param client		Client index.
 * @return				AoT class index
 */
native AOT_Team:AOT_GetTeam(client);

/**
 * Retrieves the player's cash.
 *
 * @param client		Client index.
 * @return				Player's Cash.
 */
native AOT_GetCash(client);

/**
 * Sets the player's cash.
 *
 * @param client		Client index.
 */
native AOT_SetCash(client, cash);


//MGear Stuff
/**
 * Retrieves the player's gas value.
 *
 * @param client		Client index.
 * @return				iGas
 */
native AOT_GetGas(client);

/**
 * Sets the player's gas value. (0 to max it out)
 *
 * @param client		Client index.
 * @param gas			Gas value, set to 0 to max.
 */
native AOT_SetGas(client, gas);

/**
 * Adds/Subtracts to the player's gas variable
 *
 * @param client		Client index.
 * @param gas			Gas value to modify (this makes it easier then to GetGas in the SetGas native.)
 */
native AOT_ModifyGas(client, gas);
