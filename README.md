Attack On Titan:
=======
A TF2 Modification (Gamemode Idea)

Overview:
--
    Based on the Hit anime tv show "Attack on Titan" where giant human-like creatures threaten the extinction of the human race.
	Humans were forced to hide behind giant walls that separated the titans from getting to the castle.

How To Play:
==
Players can be either a Titan or a Human (There will my team scrambling)
There will also be NPC Titans (which are stupid).

Humans: (Red)
--
	Survey Corps (Soldier): Has ken weapons, and maneuvering gear.
    Artillery (Demoman): Has cannon, and maneuvering gear. (Takes slightly more to be pulled.
	Support (Sniper): Dosent really do any damage, but can help slow down Titans. (does criticals on headshots.)

Titans: (Blu)
---
    Player controlled titans are known as a 'Deviant Titan', which is a titan that shows intelligence.
    Players can also have a chance to be a Shifter. This might come along later on in the mod showcase.
    Shifters will be in human form, and will start off as a human, but they are somewhat neutral.
    By neutral I mean that they can kill whomever the hell they want. It will probably just set the team when they attack the opposing player.

Weapons & Items:
---
weapons and items will depend on the class player's pick.


* ** Maneuvering Gear: **
	    There will be a meter for Gas levels. Certain classes will use more gas than others.
    	When you shoot the maneoviring gear, it will shoot two projectiles with ropes attached. if both projectiles hit a wall then it will act as a rope.
	    Holding CTRL whilst attached to a wall will pull you towards the wall (this uses alot less gas)
	    Holding SPACE whilst attached to a wall will push you towards the direction you are facing. (this uses up much more gas)

* ** Ken: **
		This will just be the half zatoichi for the soldier and demoman.
		Ken is a very strong metal, but against the titans they have a short durability.
		Lets say their durability is 1000, so everytime you do damage to something then it will take dmg*0.675 away from the sword's durability.
		Durability will modify the damage done to the titans.
		But don't worry about not having a weapon. you have a total of 6 Extra blades.
		Does fulltime damage attacking the head.
		
* ** Cannon: **
		This is a modfied loose cannon weapon.
		The cannon ball is alot faster, more explosive, and doesnt have a timed explosion.
		The cannon ball will explode on contact.
